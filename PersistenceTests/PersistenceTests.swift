//
//  PersistenceTests.swift
//  PersistenceTests
//
//  Created by Evgeniy Abashkin on 23/02/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import XCTest
import Realm
import RealmSwift
import RxSwift
@testable import Persistence

public enum EnumType: Int, Codable {
    case first
    case second
}

public struct Entity: Codable, Hashable {
    public var id: String
    public var stringValue: String
    public var optionalValue: String?
    public var urlValue: URL?
    public var doubleValue: Double
    public var enumValue: EnumType

    public init(
        id: String,
        stringValue: String,
        optionalValue: String?,
        urlValue: URL?,
        doubleValue: Double,
        enumValue: EnumType
    ) {
        self.id = id
        self.stringValue = stringValue
        self.optionalValue = optionalValue
        self.urlValue = urlValue
        self.doubleValue = doubleValue
        self.enumValue = enumValue
    }

    static var stub = Entity(
        id: "FFFF-0000",
        stringValue: "StringValue",
        optionalValue: nil,
        urlValue: URL(string: "http://ya.ru"),
        doubleValue: 1,
        enumValue: .first
    )
}

public final class ManagedEntity: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var stringValue: String = ""
    @objc public dynamic var optionalValue: String? = nil
    @objc public dynamic var urlStringValue: String? = nil
    @objc public dynamic var doubleValue: Double = 0
    @objc public dynamic var enumValue: Int = EnumType.first.rawValue

    public override class func primaryKey() -> String? {
        return "id"
    }
}

extension Entity: ManagedTransformable {
    public var managedObject: ManagedEntity {
        return ManagedEntity(plain: self)
    }

    public init(managed object: ManagedEntity) {
        self.init(
            id: object.id,
            stringValue: object.stringValue,
            optionalValue: object.optionalValue,
            urlValue: object.urlStringValue.flatMap { URL(string: $0) },
            doubleValue: object.doubleValue,
            enumValue: EnumType(rawValue: object.enumValue) ?? EnumType.first
        )
    }
}

extension ManagedEntity: PlainTransformable {
    public var plainObject: Entity {
        return Entity(managed: self)
    }

    public convenience init(plain object: Entity) {
        self.init()
        set(with: object)
    }

    @discardableResult
    public func set(with plain: Entity) -> ManagedEntity {
        id = plain.id
        stringValue = plain.stringValue
        optionalValue = plain.optionalValue
        urlStringValue = plain.urlValue?.absoluteString
        doubleValue = plain.doubleValue
        enumValue = plain.enumValue.rawValue
        return self
    }
}


class PersistenceTests: XCTestCase {

    private var disposeBag = DisposeBag()

    private let configuration = Realm.Configuration(inMemoryIdentifier: "com.persistence")
    private let scheduler = SerialDispatchQueueScheduler(internalSerialQueueName: "com.persistence")
    private let listenScheduler = MainScheduler.asyncInstance

    private var inMemoryHolder: Realm!

    private func createPersistence() -> PersistenceGateway {
        return PersistenceGatewayImp(
            configuration: configuration,
            scheduler: scheduler,
            listenScheduler: listenScheduler
        )
    }

    override func setUp() {
        inMemoryHolder = try! Realm(configuration: configuration)
    }

    override func tearDown() {
        disposeBag = DisposeBag()
        inMemoryHolder = nil
    }

    /// Persistence testing helper
    private func performTests(tests: (PersistenceGateway) -> [Disposable]) {
        let schedulerExpectation = expectation(description: "schedulerExpectation")

        let persistence = createPersistence()
        Disposables.create(tests(persistence)).disposed(by: disposeBag)

        Single.just((), scheduler: scheduler)
            .subscribe(onSuccess: schedulerExpectation.fulfill)
            .disposed(by: disposeBag)

        wait(for: [schedulerExpectation], timeout: 1)
    }

    func testEntity() {
        let location = Entity.stub

        performTests { persistence in
            return [
                persistence
                    .save(object: location)
                    .subscribe(),

                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, 1)
                    }),

                persistence
                    .get()
                    .subscribe(onSuccess: { (result: Entity?) in
                        XCTAssertEqual(result?.stringValue, "StringValue")
                    }),

                persistence
                    .get { $0 }
                    .subscribe(onSuccess: { (result: Entity?) in
                        XCTAssertEqual(result?.stringValue, "StringValue")
                    }),

                persistence
                    .delete(object: location)
                    .subscribe(),

                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, 0)
                    }),
            ]
        }
    }

    func testEntities() {
        let locations = [Entity](repeating: Entity.stub, count: 5).enumerated().map { pair -> Entity in
            var element = pair.element
            element.id = element.id + "-\(pair.offset)"
            return element
        }

        performTests { persistence in
            return [
                persistence
                    .save(objects: locations)
                    .subscribe(),

                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, locations.count)
                    }),

                persistence
                    .getArray()
                    .subscribe(onSuccess: { (result: [Entity]) in
                        XCTAssertEqual(result, locations)
                    }),

                persistence
                    .getArray { $0 }
                    .subscribe(onSuccess: { (result: [Entity]) in
                        XCTAssertEqual(result, locations)
                    }),

                persistence
                    .getArray { $0.filter("id = %@", "FFFF-0000-1") }
                    .subscribe(onSuccess: { (result: [Entity]) in
                        XCTAssertEqual(result[0].id, "FFFF-0000-1")
                        XCTAssertEqual(result.count, 1)
                    }),

                persistence
                    .delete(Entity.self) { $0.filter("id = %@", "FFFF-0000-1") }
                    .subscribe(),

                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, 4)
                    }),

                persistence
                    .delete(objects: locations)
                    .subscribe(),

                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, 0)
                    })
            ]
        }
    }
    
    func testClean() {
        let locations = [Entity](repeating: .stub, count: 5).enumerated().map { pair -> Entity in
            var element = pair.element
            element.id = element.id + "-\(pair.offset)"
            return element
        }
        
        performTests { persistence in
            return [
                persistence
                    .save(objects: locations)
                    .subscribe(),
                
                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, locations.count)
                    }),
                
                persistence
                    .clean()
                    .subscribe(),
                
                persistence
                    .count(Entity.self)
                    .subscribe(onSuccess: { count in
                        XCTAssertEqual(count, 0)
                    }),
            ]
        }
    }
}
