//
//  PersistenceGatewayImp.swift
//  Persistence
//
//  Created by Evgeniy Abashkin on 23/02/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift


private func isNeedUpdateObject<T: ManagedTransformable>(_: T) -> Bool {
    return T.ManagedType.primaryKey() != nil
}

public class PersistenceGatewayImp: PersistenceGateway {
    private let configuration: Realm.Configuration
    private let scheduler: SchedulerType
    private let listenScheduler: SchedulerType

    public init(
        configuration: Realm.Configuration,
        scheduler: SchedulerType,
        listenScheduler: SchedulerType
    ) {
        self.configuration = configuration
        self.scheduler = scheduler
        self.listenScheduler = listenScheduler
    }

    private func realm(scheduler: SchedulerType) -> Single<Realm> {
        return Single.just((), scheduler: scheduler)
            .map { [configuration] in try Realm(configuration: configuration) }
    }

    private func realm() -> Single<Realm> {
        return realm(scheduler: scheduler)
    }

    private func objects<T: ManagedTransformable>(
        _: T.Type,
        scheduler: SchedulerType,
        block: @escaping ResultsBlock<T>
    ) -> Single<Results<T.ManagedType>> {
        return realm(scheduler: scheduler)
            .map { $0.objects(T.ManagedType.self) }
            .map { block($0) }
    }

    public func count<T: ManagedTransformable>(
        _: T.Type,
        block: @escaping ResultsBlock<T>
    ) -> Single<Int> {
        return objects(T.self, scheduler: scheduler, block: block)
            .map { $0.count }
    }

    public func get<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Single<T?> {
        return objects(T.self, scheduler: scheduler, block: block)
            .map { $0.first?.plainObject }
    }

    public func getArray<T: ManagedTransformable>(
        range: Range<Int>?,
        block: @escaping ResultsBlock<T>
    ) -> Single<[T]> {
        return objects(T.self, scheduler: scheduler, block: block)
            .map { results in range.map { Array(results[$0.clamped(to: 0 ..< results.count)]) } ?? Array(results) }
            .map { $0.map { $0.plainObject } }
    }

    public func listen<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Observable<T?> {
        return objects(T.self, scheduler: listenScheduler, block: block)
            .asObservable()
            .flatMap { Observable.collection(from: $0) }
            .map { $0.first?.plainObject }
    }

    public func listenArray<T: ManagedTransformable>(
        range: Range<Int>?,
        block: @escaping ResultsBlock<T>
    ) -> Observable<[T]> {
        return objects(T.self, scheduler: listenScheduler, block: block)
            .asObservable()
            .flatMap { Observable.collection(from: $0) }
            .map { objects in range.map { Array(objects[$0.clamped(to: 0 ..< objects.count)]) } ?? Array(objects) }
            .map { $0.map { $0.plainObject } }
    }

    public func save<T: ManagedTransformable>(object: T) -> Single<Void> {
        return save(objects: [object])
    }

    public func save<T: ManagedTransformable>(objects: [T]) -> Single<Void> {
        guard let firstObject = objects.first else {
            return Single.error(PersistenceError.emptyArray)
        }

        return realm()
            .map { realm in
                try realm.write {
                    let isNeedUpdate = isNeedUpdateObject(firstObject)
                    let managedObjects = objects.map { $0.managedObject }
                    realm.add(managedObjects, update: isNeedUpdate)
                }

                return ()
        }
    }

    public func delete<T: ManagedTransformable>(object: T) -> Single<Void> {
        return delete(objects: [object])
    }

    public func delete<T: ManagedTransformable>(objects: [T]) -> Single<Void> {
        guard let primaryKey = T.ManagedType.primaryKey() else {
            return .error(PersistenceError.noPrimaryKeyInClass)
        }

        return realm()
            .flatMap { realm in
                let primaryKeyValues = objects.compactMap { $0.managedObject.value(forKey: primaryKey) }
                guard !primaryKeyValues.isEmpty else {
                    return .error(PersistenceError.noPrimaryKeyValue)
                }

                let predicate = NSPredicate(format: "\(primaryKey) IN %@", primaryKeyValues)

                try realm.write {
                    let objectsToDrop = realm.objects(T.ManagedType.self).filter(predicate)
                    realm.delete(objectsToDrop)
                }

                return .just(())
        }
    }

    public func delete<T: ManagedTransformable>(
        _: T.Type,
        block: @escaping ResultsBlock<T>
    ) -> Single<Void> {
        return objects(T.self, scheduler: scheduler, block: block)
            .do(onSuccess: { results in
                try results.realm?.write {
                    results.forEach { results.realm?.delete($0) }
                }
            })
            .map { _ in () }
    }
    
    public func clean() -> Single<Void> {
        return realm()
            .map { realm in
                try realm.write {
                    realm.deleteAll()
                }
            }
    }
}
