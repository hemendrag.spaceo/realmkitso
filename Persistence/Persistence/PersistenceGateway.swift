//
//  PersistenceGateway.swift
//
//  Created by Vladislav Sedinkin on 20.09.2018.
//  Copyright © 2018 SMediaLink. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

enum PersistenceError: Error {
    case emptyArray
    case noPrimaryKeyInClass
    case noPrimaryKeyValue
}

public typealias ResultsBlock<T: ManagedTransformable> = (Results<T.ManagedType>) -> Results<T.ManagedType>

public protocol PersistenceGateway {
    func count<T: ManagedTransformable>(
        _ type: T.Type,
        block: @escaping ResultsBlock<T>
    ) -> Single<Int>

    func get<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Single<T?>

    func getArray<T: ManagedTransformable>(
        range: Range<Int>?,
        block: @escaping ResultsBlock<T>
    ) -> Single<[T]>

    func listen<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Observable<T?>

    func listenArray<T: ManagedTransformable>(
        range: Range<Int>?,
        block: @escaping ResultsBlock<T>
    ) -> Observable<[T]>

    func save<T: ManagedTransformable>(object: T) -> Single<Void>

    func save<T: ManagedTransformable>(objects: [T]) -> Single<Void>

    func delete<T: ManagedTransformable>(object: T) -> Single<Void>

    func delete<T: ManagedTransformable>(objects: [T]) -> Single<Void>

    func delete<T: ManagedTransformable>(
        _ type: T.Type,
        block: @escaping ResultsBlock<T>
    ) -> Single<Void>
    
    func clean() -> Single<Void>
}

extension PersistenceGateway {
    public func get<T: ManagedTransformable>() -> Single<T?> {
        return get(block: { $0 })
    }

    public func count<T: ManagedTransformable>(
        _ type: T.Type
    ) -> Single<Int> {
        return count(type, block: { $0 })
    }

    public func getArray<T: ManagedTransformable>() -> Single<[T]> {
        return getArray(range: nil, block: { $0 })
    }

    public func getArray<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Single<[T]> {
        return getArray(range: nil, block: block)
    }

    public func listen<T: ManagedTransformable>() -> Observable<T?> {
        return listen(block: { $0 })
    }

    public func listenArray<T: ManagedTransformable>() -> Observable<[T]> {
        return listenArray(range: nil, block: { $0 })
    }

    public func listenArray<T: ManagedTransformable>(
        block: @escaping ResultsBlock<T>
    ) -> Observable<[T]> {
        return listenArray(range: nil, block: block)
    }
}
